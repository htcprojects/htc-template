package com.htc.ea.util;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public final class SingleData {
	private static SingleData INSTANCE;
    private String remoteAddress;
     
    private SingleData() {remoteAddress="unknownHost";}
     
    public static SingleData getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new SingleData();
        }
        return INSTANCE;
    }

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}
    
    
}

