package com.htc.ea.util;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.dto.ResponseGraph;
import com.htc.ea.exception.GraphQLConsumeException;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
@Component
public class GraphQLConsumerUtil {

	private static Logger logback = LoggerFactory.getLogger(GraphQLConsumerUtil.class);
	
	//TODO agregar la categoria a las event category
	private static final String CATEGORY_CONSUMER 	= "consumer";
	//TODO agregar key 615 en config entry y de valor msj de error
	private static final String ERROR_EMPTY_VALUES = "615";
	private static final String ERROR_PREPARING_REQUEST = "616";
	
	//HEADERS
	private static final String HEADER_SEQUENCE 		= "until-last-sequence";
	private static final String HEADER_REFERENCE_ID 	= "operation-reference-id";
	private static final String HEADER_END_USER 		= "end-user";
	
	//TODO agregar bean restTemplate en config
	@Autowired
	private RestTemplate restTemplate;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	
	
	@PostConstruct
	public void init() {
		mapper.setSerializationInclusion(Include.NON_NULL);
	}
	
	public String consumeGraphQL(String url,GraphQLOperation operation,String method,Map<String, Object> requestObject,String... values) {
		ResponseGraph responseGraph;
		try {
			StopWatch timer = new StopWatch();
			//timer.start();
			if(url==null || url.isEmpty() || method==null || method.isEmpty() || requestObject==null || values==null || values.length==0) {
				throw new GraphQLConsumeException(ERROR_EMPTY_VALUES);
			}
			Optional<String> resultPrepareObject = prepareRequestObject(requestObject);
			if(!resultPrepareObject.isPresent()) {
				throw new GraphQLConsumeException(ERROR_PREPARING_REQUEST);
			}
			String preparedObject 	= resultPrepareObject.get();
			
			RequestData requestData = prepareRequest(operation.getValue(), method, preparedObject, values);
			System.out.println("REQUEST\n"+mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestData));
			HttpHeaders headers 	= new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		    //headers.add(HEADER_END_USER, "end user");
		    //headers.add(HEADER_REFERENCE_ID, "reference id");
		    //headers
		    
		    //HttpEntity<RequestData> request 		= new HttpEntity<>(requestData,headers);
		    //ResponseEntity<String> response 	= restTemplate.exchange(url, HttpMethod.POST, request, String.class);
			
		    //System.out.println("Result Graph "+response.getBody());
		    //return response.getBody();
		    //responseGraph = response.getBody();
		} catch (GraphQLConsumeException graphEx) {
			logback.error("ERROR GRAPHQL {} ",graphEx.getServiceCode());
			// TODO: handle exception
		}catch (Exception ex) {
			logback.error("ERROR  {} ",ex);
			// TODO: handle exception
		}
		return "";
	}
	
	public Optional<String> prepareRequestObject(Map<String, Object> request) {
		String jsonString=null;
		try {
			jsonString = mapper.writeValueAsString(request);
			jsonString = jsonString.replaceAll("\"([^\"]+)\":", "$1:");
			jsonString = jsonString.substring(1, jsonString.length()-1);
		} catch (JsonProcessingException e) {
			logback.error("ERROR PARSING OBJECT: \n {}",e);
		}
		return Optional.of(jsonString);	
	}
	
	private RequestData prepareRequest(String operation,String method,String preparedObject,String... values) {
		String preparedValues = Arrays.toString(values).replace('[','{').replace(']', '}');
		String query = operation+"{"+method+"("+preparedObject+")"+preparedValues+"}";
		return new RequestData(query);
	}
	
	class RequestData{
		@JsonProperty("query")
		private String query;
		
		public RequestData(String query) {
			super();
			this.query = query;
		}

		public String getQuery() {
			return query;
		}

		public void setQuery(String query) {
			this.query = query;
		}	
	}
}

	