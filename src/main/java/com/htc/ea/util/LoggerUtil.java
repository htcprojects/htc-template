package com.htc.ea.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.log.EventData;
import com.htc.ea.util.log.EventLevel;
import com.htc.ea.util.log.LoggingServiceFacade;
import com.htc.ea.util.parsing.ParsingUtil;
import com.htc.ea.util.util.TransactionIdUtil;

/**
 * The Class AppUtil.
 */
@Component
public class LoggerUtil {

	private static final String STRING = "String";
	private static final String ERROR_LOGGER_UTIL = "Error LoggerUtil {}";
	private static Logger logback = LoggerFactory.getLogger(LoggerUtil.class);
	@Autowired
	private Environment env;
	@Autowired
	private LoggingServiceFacade logger;
	@SuppressWarnings("unused")
	@Autowired
	private ParsingUtil parsingUtil;

	private static final String UNKNOWNHOST = "unknownHost";

	/**
	 * Logeo para realizar procesos de consumos de servicios
	 * Se establece el EventLevel como INFO
	 * Si se debe establecer parametro en que defina el tipo de dato a parsear el objeto
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param data objeto que se parseara a json por defecto
	 * @param msg mensaje descriptivo del evento
	 * @param detail informacion adicional
	 * @param duration duracion del evento
	 * @param dataType tipo de dato a convertir el objeto
	 */
	public void info(String category,Object data ,String msg, String detail, Long duration,String dataType){
		try{
			if (logging()){

				//si viene un String o arreglo de String
				GenericDto genericDto = null;
				if(data!=null && (data instanceof String || data instanceof String[] )) {
					genericDto = new GenericDto();
					genericDto.put(STRING, data);
					data = genericDto;
				}
				
				String endUserLocation 	= SingleData.getInstance().getRemoteAddress();
				String serverLocation 	= InetAddress.getLocalHost().getHostAddress();
				String serviceName		= getMethodName();
				String sequence 		= TransactionIdUtil.nextSequenceId();
				String msisdn			= TransactionIdUtil.getMsisdn();
				String refId 			= //TransactionIdUtil.getId();//datos quemado
				dataType 				= data!=null ? dataType : null;
				String clazz			= getClassName();
				
				logger.log(EventData.builder()
						.category(category)
						.level(EventLevel.INFO.toString())
						.endUserLocation(endUserLocation)
						.serverLocation(serverLocation)
						.endUser(msisdn)
						.source(clazz)
						.name(serviceName)
						.message(msg)
						.detail(detail)
						.originReferenceId(refId)
						.referenceId(refId)
						.duration(duration)
						.automatic(false)
						.successful(true)
						.sequence(sequence)
						.data(data)
						.dataType(dataType)
						.build());
			}
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}
	
	
	/**
	 * Logeo para realizar procesos de consumos de servicios
	 * Se establece el EventLevel como INFO
	 * Si se envia valor en parametro data parsea por defecto a json
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param data objeto que se parseara a json por defecto
	 * @param msg mensaje descriptivo del evento
	 * @param detail informacion adicional
	 * @param duration duracion del evento
	 */
	public void info(String category,Object data,String msg, String detail, Long duration){
		try{
			if (logging()){
				
				//si viene un String o arreglo de String
				GenericDto genericDto = null;
				if(data!=null && (data instanceof String || data instanceof String[] )) {
					genericDto = new GenericDto();
					genericDto.put(STRING, data);
					data = genericDto;
				}
				
				String endUserLocation 	= SingleData.getInstance().getRemoteAddress();
				String serverLocation 	= InetAddress.getLocalHost().getHostAddress();
				String serviceName		= getMethodName();
				String sequence 		= TransactionIdUtil.nextSequenceId();
				String dataType 		= data!=null ? "json" : null;
				String msisdn			= TransactionIdUtil.getMsisdn();
				String refId 			= TransactionIdUtil.getId();
				String clazz			= getClassName();
				
				logger.log(EventData.builder()
						.category(category)
						.level(EventLevel.INFO.toString())
						.endUserLocation(endUserLocation)
						.serverLocation(serverLocation)
						.endUser(msisdn)
						.source(clazz)
						.name(serviceName)
						.message(msg)
						.detail(detail)
						.originReferenceId(refId)
						.referenceId(refId)
						.duration(duration)
						.automatic(false)
						.successful(true)
						.sequence(sequence)
						.data(data)
						.dataType(dataType)
						.build());
			}
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}

	/**
	 * Logeo para debugear procesos generales en la aplicacion
	 * Se establece el EventLevel como DEBUG
	 * Si se envia valor en parametro data parsea por defecto a json
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param data objeto que se parseara a json por defecto
	 * @param msg mensaje descriptivo del evento
	 * @param detail informacion adicional
	 * @param duration duracion del evento
	 */
	public void debug(String category,Object data, String msg, String detail, Long duration){
		try{
			if (logging()){
				
				//si viene un String o arreglo de String
				GenericDto genericDto = null;
				if(data!=null && (data instanceof String || data instanceof String[] )) {
					genericDto = new GenericDto();
					genericDto.put(STRING, data);
					data = genericDto;
				}
				
				String endUserLocation 	= SingleData.getInstance().getRemoteAddress();
				String serverLocation 	= InetAddress.getLocalHost().getHostAddress();
				String serviceName		= getMethodName();
				String sequence 		= TransactionIdUtil.nextSequenceId();
				String dataType 		= data!=null ? "json" : null;
				String msisdn			= TransactionIdUtil.getMsisdn();
				String refId 			= TransactionIdUtil.getId();
				String clazz			= getClassName();
				
				logger.log(EventData.builder()
						.category(category)
						.level(EventLevel.DEBUG.toString())
						.endUserLocation(endUserLocation)
						.serverLocation(serverLocation)
						.endUser(msisdn)
						.source(clazz)
						.name(serviceName)
						.message(msg )
						.detail(detail)
						.originReferenceId(refId)
						.referenceId(refId)
						.duration(duration)
						.automatic(false)
						.successful(true)
						.sequence(sequence)
						.data(data)
						.dataType(dataType)
						.build());
			}
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}

	
	/**
	 * Logeo para errores con excepciones
	 * Se establece el EventLevel como ERROR
	 * Si se envia valor en parametro data parsea por defecto a json
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param data objeto que se parseara a json por defecto
	 * @param msg mensaje descriptivo del evento
	 * @param detail informacion adicional
	 * @param duration duracion del evento
	 * @param responseCode codigo de respuesta obtenido en el error
	 * @param exception excepcion generada
	 */
	public void error(String category,Object data,String msg, String detail, Long duration, String responseCode, Throwable exception) {
		try{
			if (logging()){
				
				//si viene un String o arreglo de String
				GenericDto genericDto = null;
				if(data!=null && (data instanceof String || data instanceof String[] )) {
					genericDto = new GenericDto();
					genericDto.put(STRING, data);
					data = genericDto;
				}
				
				String endUserLocation 	= SingleData.getInstance().getRemoteAddress();
				String serverLocation 	= InetAddress.getLocalHost().getHostAddress();
				String serviceName		= getMethodName();
				String sequence 		= TransactionIdUtil.nextSequenceId();
				String msisdn			= TransactionIdUtil.getMsisdn();
				String refId 			= TransactionIdUtil.getId();
				String clazz			= getClassName();
				
				logger.log(EventData.builder()
						.category(category)
						.level(EventLevel.ERROR.toString())
						.endUserLocation(endUserLocation)
						.serverLocation(serverLocation)
						.endUser(msisdn)
						.source(clazz)
						.name(serviceName)
						.message(msg)
						.detail(detail)
						.responseCode(responseCode)
						.originReferenceId(refId)
						.referenceId(refId)
						.duration(duration)
						.automatic(false)
						.successful(true)
						.data(data)
						.exception(exception)
						.sequence(sequence)
						.build());
			}
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}


	/**
	 * Creates the generic response.
	 *
	 * @param operationReferenceId the operation reference id
	 * @param responseCode the response code
	 * @param responseMessage the response message
	 * @param data the data
	 * @param endUser the end user
	 * @return the generic dto
	 */
	public GenericDto createGenericResponse(String operationReferenceId, String responseCode, String responseMessage, String data, String endUser){
		GenericDto response = new GenericDto();
		
		response.setProperty("platform", env.getProperty("target.key"));
		response.setProperty("operationReferenceId", operationReferenceId);
		response.setProperty("responseCode", responseCode);
		response.setProperty("responseMessage", responseMessage);
		response.setProperty("data", data);//TODO da error en algunos casos de parseo al dejar la data como vacia
		response.setProperty("endUser", endUser);
		
		return response;
	}
	

	/**
	 * Gets the server location.
	 *
	 * @return the server location
	 */
	public static String getServerLocation(){
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
		return UNKNOWNHOST;
	}


	/**
	 * Logging.
	 *
	 * @return true, if successful
	 */
	public boolean logging(){
		return Boolean.parseBoolean(env.getProperty("logging.enabled"));
	}
	

	
	private String getClassName() {
		return Thread.currentThread().getStackTrace()[3].getClassName();
	}
	
	/**
	 * Gets the method name.
	 *
	 * @return the method name
	 */
	private String getMethodName() {
		return Thread.currentThread().getStackTrace()[3].getMethodName();
	}
	
	/**
	 * Gets the property.
	 *
	 * @param key the key
	 * @return the property
	 */
	public String getProperty(String key) {
		return env.getProperty(key);
	}
	
	
	
}
