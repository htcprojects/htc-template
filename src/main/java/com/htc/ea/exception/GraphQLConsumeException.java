package com.htc.ea.exception;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class GraphQLConsumeException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -346306685068567008L;
	private String serviceCode;
	
	public GraphQLConsumeException(String serviceCode) {
		super();
		this.serviceCode = serviceCode;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
}
