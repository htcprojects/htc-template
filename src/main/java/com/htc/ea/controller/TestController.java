package com.htc.ea.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.validator.internal.util.privilegedactions.NewInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.htc.ea.dto.AccountDto;
import com.htc.ea.dto.AllLink;
import com.htc.ea.dto.RequestDto;
import com.htc.ea.dto.ResultGraphTest;
import com.htc.ea.mcs.consumer.ConsumerOperation;
import com.htc.ea.mcs.consumer.ConsumerRestHelper;
import com.htc.ea.mcs.consumer.ServiceException;
import com.htc.ea.mcs.consumer.ServiceRequest;
import com.htc.ea.mcs.consumer.ServiceResponse;
import com.htc.ea.util.GraphQLConsumerUtil;
import com.htc.ea.util.GraphQLOperation;
import com.htc.ea.util.LoggerUtil;
import com.htc.ea.util.SingleData;
import com.htc.ea.util.util.TransactionIdUtil;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
@RestController
public class TestController {

	@Autowired
	private LoggerUtil logger;

	@Autowired
	private ConsumerRestHelper consumerRestHelper;

	// consumo de graphql con restTemplate
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private GraphQLConsumerUtil graphQLConsumer;
	
	private String getGraphQL() {
		// ya funciona poniendo %7B para { y %7D para }
		// String resource =
		// "http://216eb5b4.ngrok.io/graphql/?query=%7BallLinks%7Bid%7D%7D";

		String resource = "http://216eb5b4.ngrok.io/graphql/";

		Map<String, String> map = new HashMap<>();

		try {
			// map.put("&query", URLEncoder.encode("{allLinks{id}}","UTF-8"));
			map.put("&query", URLEncoder.encode("{\r\n" + "  allLinks{\r\n" + "    id\r\n" + "  }\r\n" + "}", "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource,
				ConsumerOperation.GET);

		ServiceResponse<ResultGraphTest> serviceResponse = new ServiceResponse<>();
		serviceRequest.setData(map);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("msisdn");
		serviceRequest.setOperationRefId("123123");

		serviceRequest.getParameters().put("Content-Type", "application/json");

		String result = "";
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, ResultGraphTest.class);

			for (AllLink link : serviceResponse.getData().getData().getAllLinks()) {
				System.out.println("ID: " + link.getId());
				result += link.getId() + "\n";
			}

		} catch (ServiceException e) {
			result = "Error";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	private String postGraphQL() {

		String resource = "http://216eb5b4.ngrok.io/graphql/";

		Map<String, String> map = new HashMap<>();

		map.put("query", "mutation{createLink(url:\"test\",description:\"desc\"){id}}");

		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource,
				ConsumerOperation.POST);

		ServiceResponse<ResultGraphTest> serviceResponse = new ServiceResponse<>();
		serviceRequest.setData(map);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("customer-id");
		serviceRequest.getParameters().put("Content-Type", "application/json");

		String result = " ";
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, ResultGraphTest.class);

			System.out.println("ID: " + serviceResponse.getData().getData().getCreateLink().getId());
			result += serviceResponse.getData().getData().getCreateLink().getId() + "\n";

		} catch (ServiceException e) {
			result = " Error";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	private String queryPostAccount() {
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	    
	    String url = "http://localhost:8030/development/ewallet/graphql";
	    
	    RestTemplate restTemplate = new RestTemplate();
	    
	    String body = "{\r\n" + 
	    		"  \"query\":\"query{\r\n" + 
	    		"    accountById(id:\\\"562024403619\\\"){\r\n" + 
	    		"      alias,limitList{\r\n" + 
	    		"        prefix,min,max\r\n" + 
	    		"      }\r\n" + 
	    		"    }\r\n" + 
	    		"  }\"\r\n" + 
	    		"}";
	    
	    String bodyMutation ="mutation{\r\n" + 
	    		"  saveAccounts(accounts:[\r\n" + 
	    		"    {\r\n" + 
	    		"      accountType:\"BPRN\",\r\n" + 
	    		"      alias:\"TEST_ACCOUNT 1\",\r\n" + 
	    		"      description:\"new Test account 1\"\r\n" + 
	    		"    },\r\n" + 
	    		"    {\r\n" + 
	    		"      accountType:\"BCPR\",\r\n" + 
	    		"      alias:\"TEST_ACCOUNT 2\",\r\n" + 
	    		"      description:\"new Test account 2\"\r\n" + 
	    		"    },\r\n" + 
	    		"    {\r\n" + 
	    		"      accountType:\"BEMP\",\r\n" + 
	    		"      alias:\"TEST_ACCOUNT\",\r\n" + 
	    		"      description:\"new Test account 3\"\r\n" + 
	    		"    }\r\n" + 
	    		"  ]){\r\n" + 
	    		"    code\r\n" + 
	    		"    detail\r\n" + 
	    		"    value\r\n" + 
	    		"  }\r\n" + 
	    		"}";
	    
	    
	    
	    RequestDto requestDto = new RequestDto();
	    //requestDto.setQuery("query{accountById(id:\"562024403619\"){alias,limitList{prefix,min,max}}}");
	    requestDto.setQuery(bodyMutation);
	    
	    HttpEntity<RequestDto> request = new HttpEntity<>( requestDto  );
	    ResponseEntity<String> response = restTemplate
	    		.exchange(url, HttpMethod.POST, request, String.class);


	    
	    return response.getBody();//restTemplate.exchange("http://localhost:8080/products", HttpMethod.GET, entity, String.class).getBody();
		
	}
	
	
	private String testGraphQLConsumer() {
		

	    String url = "http://localhost:8030/development/ewallet/graphql";
	    String method= "saveAccounts";
	    List<AccountDto> requestObject = new ArrayList<>();
	    
		String types[] = {"BPRN","BCPR","BEMP"};//prefijos de tipos de cuentas
	    
		for (int i = 0; i < 3; i++) {
			AccountDto inputAccount  = new AccountDto();
			//inputAccount.setAccountId("0-00000-0"+i);
			
			inputAccount.setAccountType(types[i]);
			inputAccount.setAlias("test_account "+i);
			inputAccount.setDescription("Cuenta de prueba "+i);
			
			requestObject.add(inputAccount);
		}
		String[] values = {"code","detail","value"};
	    
		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("accounts", requestObject);
		
		return graphQLConsumer.consumeGraphQL(url, GraphQLOperation.MUTATION, method, requestMap, values);
	}

	@GetMapping("/")
	public String test() {

		String result = "QUERY ";

		TransactionIdUtil.begin();
		SingleData.getInstance().setRemoteAddress("127.0.0.1");
		TransactionIdUtil.setMsisdn("77462572");

		logger.debug("service", null, "Hola", "Hola mundo", null);

		
		//result += "\n"+queryPostAccount();
		
		//result += getGraphQL();

		//result += " POST ";

		//result += postGraphQL();

		result+="\n"+testGraphQLConsumer();
		
		TransactionIdUtil.end();

		return "Result " + result;
	}
}
