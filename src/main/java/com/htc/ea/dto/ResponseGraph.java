package com.htc.ea.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class ResponseGraph {

	@JsonProperty("data")
	private Object data;
	@JsonProperty("errors")
	private Map<String, Object> errors;
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Map<String, Object> getErrors() {
		return errors;
	}
	public void setErrors(Map<String, Object> errors) {
		this.errors = errors;
	}
}
