package com.htc.ea.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class RequestDto {
	@JsonProperty("query")
	private String query;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}	
}
