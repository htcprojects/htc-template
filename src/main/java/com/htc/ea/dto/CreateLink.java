package com.htc.ea.dto;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class CreateLink {
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
