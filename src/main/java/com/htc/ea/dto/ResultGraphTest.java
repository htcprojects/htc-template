package com.htc.ea.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class ResultGraphTest {
	@JsonProperty("data")
	private Data DataObject;
	public Data getData() {
		return DataObject;
	}
	public void setData(Data dataObject) {
		this.DataObject = dataObject;
	}
}