package com.htc.ea.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class AccountDto {
	
	private String accountId;	
	private String accountType;
	private String alias;
	private BigDecimal balance;
	private BigDecimal withholdingBalance;
	private String description;
	private String status;
	private String createdDate;
	private String updatedDate;
	
	public BigDecimal getWithholdingBalance() {
		return withholdingBalance;
	}

	public void setWithholdingBalance(BigDecimal withholdingBalance) {
		this.withholdingBalance = withholdingBalance;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}


	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountDto [");
		if (alias != null) {
			builder.append("alias=");
			builder.append(alias);
			builder.append(", ");
		}
		if (balance != null) {
			builder.append("balance=");
			builder.append(balance);
			builder.append(", ");
		}
		if (description != null) {
			builder.append("description=");
			builder.append(description);
			builder.append(", ");
		}
		if (status != null) {
			builder.append("status=");
			builder.append(status);
			builder.append(", ");
		}
		if (accountType != null) {
			builder.append("accountType=");
			builder.append(accountType);
		}
		builder.append("]");
		return builder.toString();
	}
	
}