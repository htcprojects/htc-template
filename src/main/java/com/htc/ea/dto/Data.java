package com.htc.ea.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class Data {
	@JsonProperty("allLinks")
	private List< AllLink > allLinks = new ArrayList<>();
	
	@JsonProperty("createLink")
	private CreateLink createLink;
	
	public CreateLink getCreateLink() {
		return createLink;
	}
	public void setCreateLink(CreateLink createLink) {
		this.createLink = createLink;
	}
	public List<AllLink> getAllLinks() {
		return allLinks;
	}
	public void setAllLinks(List<AllLink> allLinks) {
		this.allLinks = allLinks;
	}
}