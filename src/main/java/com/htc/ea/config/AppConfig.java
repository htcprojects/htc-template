package com.htc.ea.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import com.htc.ea.mcs.consumer.config.ServiceConsumerRestConfig;
import com.htc.ea.util.configuration.UtilAppConfig;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
@Configuration
@EnableAsync
@Import({ UtilAppConfig.class,ServiceConsumerRestConfig.class})
public class AppConfig {
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}

